<!-- what we do -->
	<section class="what_you">
	<div class="container">
		<div class="wthree_head_section">
				<h3 class="w3l_header">What we do</h3>
				<p>Our passion is to help you discover yours.</p>
			</div>
			
			<div class="row about-info-grids">
				<div class="col-md-4 about-info about-info1">
					<i class="far fa-gem"></i>
					<h4>Career Options</h4>
					<div class="h4-underline"></div>
					<p>Given that there are over 12,000 different careers on the planet today, choosinng the perfect one can be complex. It's just not possible to have a detailed understanding of all. We understand.</p>
				</div>
				<div class="col-md-4 about-info about-info2">
					<i class="fas fa-book"></i>
					<h4>Certified Tutors</h4>
					<div class="h4-underline"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
				<div class="col-md-4 about-info about-info3">
					<i class="fab fa-codepen"></i>
					<h4>Infant Care</h4>
					<div class="h4-underline"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="bord"></div>
			<div class="row about-info-grids">
				<div class="col-md-4 about-info about-info1">
					<i class="fas fa-university"></i>
					<h4>Certified Tutors</h4>
					<div class="h4-underline"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
				<div class="col-md-4 about-info about-info2">
					<i class="fas fa-folder"></i>
					<h4>Coaching</h4>
					<div class="h4-underline"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
				<div class="col-md-4 about-info about-info3">
					<i class="fas fa-graduation-cap"></i>
					<h4>Infant Care</h4>
					<div class="h4-underline"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
	</div>
</section>
<!-- //what we do -->